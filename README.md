# ns-matcher

[![License](https://img.shields.io/npm/l/ns-matcher.svg)](LICENSE)
[![Version](https://img.shields.io/npm/v/ns-matcher.svg)](https://www.npmjs.com/package/ns-matcher)

A simple matcher utility for namespaces (e.g. within `process.env.DEBUG`).

## Usage

```javascript
const nsMatcher = require("ns-matcher");

// multiple patterns can be separated by whitespace, commas or semicolons
const matcher = nsMatcher("app:**:log  !app:ignored:*  app:debug");

// now test for some namespaces to integrate with your application logic
if (matcher.test("app:some-module:log")) {
  console.log("Hello world!");
}

// the last matching pattern decides whether the test holds true or false
if (matcher.test("app:ignored:log")) {
  console.log("This will not be visible.");
}

// `matcher.every` and `matcher.some` are helpers for common iterations
if (matcher.every(["app:log", "app:debug"])) {
  console.log("How are you?");
}
```

## API

`{Matcher} nsMatcher([String|String[]] pattern, [[Object?] options])`

Prepares the given `pattern` to perform tests for namespaces.
If the given `pattern` is a String, it is split on `,`, `;` and whitespace characters unless `options.split` is set to
false. Available options may be looked up within
[the source](https://gitlab.com/frissdiegurke/ns-matcher/blob/master/lib/index.js).

The `Matcher` class provides the following methods:

 * `{Boolean} test(String namespace)` Tests whether the passed namespace is matched by `pattern`.
 * `{Boolean} some(String[] namespaces)` Tests whether any of the passed namespaces is matched by `pattern`.
 * `{Boolean} every(String[] namespaces)` Tests whether all of the passed namespaces are matched by `pattern`.

`Matcher#any()` and `Matcher#all()` are aliases for `Matcher#some()` and `Matcher#every()` respectively.

## Pattern

A single pattern (read as: item of passed array / split passed string) may contain wildcards `*` (match any amount of
characters that are not scope-characters) and deep wildcards `**` (match any amount of characters).
Scope-characters are `:` and `/`.

### Examples

`a` matches only `a`.

`a:b` matches `a:b`.

`a:*` matches `a:x` but not `a` or `a:x:y`.

`*:a` matches `x:a` but not `a` or `x:y:a`.

`a:*:b` matches `a:x:b` but not `a:b` or `a:x:y:b`.

`a:**` matches `a`, `a:x` and `a:x:y`.

`**:a` matches `a`, `x:a` and `x:y:a`.

`a:**:b` matches `a:b`, `a:x:b` and `a:x:y:b` but not `ab`.

### Inversion

If a pattern starts with `!` or `-`, the value of the pattern is inverted. The value of the last matching pattern is
returned by the `test()` function.

Thus one can use `a:**,!a:b:**,a:b:c` (three patterns, separated by `,`) in order to match `a`, `a:x` and `a:b:c` but
not `a:b` or `a:b:x`.
